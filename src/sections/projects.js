/** @jsx jsx */
import { jsx } from 'theme-ui';
import { Container, Grid } from 'theme-ui';
import SectionHeader from 'components/section-header';
import FeatureCard from 'components/feature-card.js';
import Blue from 'assets/feature/blue.svg';
import Yellow from 'assets/feature/yellow.svg';
import Green from 'assets/feature/green.svg';
import Purple from 'assets/feature/purple.svg';

const data = [
  {
    id: 1,
    imgSrc: Blue,
    altText: 'Blog',
    postLink: 'https://codingfinesse.com/',
    title: 'Blog',
    text:
      'My very own technical blog powered by GitHub and Angular. This blog serves a pair of purposes. First, it will allow me to document my journey to become a seasoned developer. Second, it will provide a learning by teaching platform that I can utilize to build my skills.',
  },
  {
    id: 2,
    imgSrc: Yellow,
    altText: 'Coding Portfolio',
    postLink: 'https://darrenptran.github.io',
    title: 'Coding Portfolio',
    text:
      'My coding portfolio along with general background information. It is powered by React and GitHub.',
  },
  {
    id: 3,
    imgSrc: Green,
    altText: 'TBD',
    title: 'TBD',
    text:
      'TBD',
  },
  {
    id: 4,
    imgSrc: Purple,
    altText: 'TBD',
    title: 'TBD',
    text:
      'TBD',
  },
];

export default function Feature() {
  return (
   <section sx={{ variant: 'section.feature'}} id="projects">
     <Container>
       <SectionHeader
       slogan="Projects"
       title="Project Portfolio"
       />

       <Grid sx={styles.grid}>
        {data.map((item) =>(
          <FeatureCard
          key={item.id}
          src={item.imgSrc}
          alt={item.altText}
          postLink={item.postLink}
          title={item.title}
          text={item.text}
          />
        ))}
       </Grid>
     </Container>
   </section>
  );
}

const styles = {
  grid: {
    pt: [0, null, null, null, null, null, 2],
    px: [5, 6, 0, null, 7, 8, 7],
    gridGap: [
      '40px 0',
      null,
      '45px 30px',
      null,
      '60px 50px',
      '70px 50px',
      null,
      '80px 90px',
    ],
    gridTemplateColumns: ['repeat(1,1fr)', null, 'repeat(2,1fr)'],
  },
};
