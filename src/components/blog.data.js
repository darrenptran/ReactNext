/** @jsx jsx */
import PostThumb1 from 'assets/blog/blog1.jpg';
import PostThumb2 from 'assets/blog/loading.jpg';
import PostThumb3 from 'assets/blog/loading.jpg';

export const data = [
  {
    id: 1,
    imgSrc: PostThumb1,
    altText: 'Blog Tutorial',
    postLink: '',
    title: 'Blog Tutorial',
    authorName: 'Darren P. Tran',
    date: '',
  },
  {
    id: 2,
    imgSrc: PostThumb2,
    altText: 'Filtering Tuorial',
    postLink: '#',
    title: 'Filtering Tuorial',
    authorName: 'Darren P. Tran',
    date: '',
  },
  {
    id: 3,
    imgSrc: PostThumb3,
    altText: 'TBC',
    postLink: '#',
    title: 'TBC',
    authorName: 'Darren P. Tran',
    date: '',
  },
  {
    id: 4,
    imgSrc: PostThumb2,
    altText: 'Creative',
    postLink: '#',
    title: 'Product Marketing: Creative Market',
    authorName: 'Darren P. Tran',
    date: '',
  },
];
